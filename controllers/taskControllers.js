const Task = require(`./../models/Tasks`)

// CREATE A TASK
module.exports.createTask = async (reqBody) => {
    return await Task.findOne({name: reqBody.name}).then( (result, err) => {
		if(result != null && result.name == reqBody.name){
			return true
		} else {
			if(result == null){
				let newTask = new Task({
					name: reqBody.name
				})
				return newTask.save().then( result => result )
			}
		}
	})
}

// GET ALL TASKS
module.exports.getAllTasks = async () => {
	return await Task.find().then( (result, err) => {
		if(result){
			return result
		} else {
			return err
		}
	})
}

// DELETE A TASK
module.exports.deleteTask = async (id) => {
	return await Task.findByIdAndDelete(id).then( result => {
		try{
			if(result != null){
				return true
			} else {
				return false
			}
		}catch(err){
			return err
		}
	})
}

// UPDATE A TASK
module.exports.updateTask = async (name, reqBody) => {
	return await Task.findOneAndUpdate({name: name}, {$set: reqBody}, {new:true})
	.then(response => {
		if(response == null){
			return {message: `no existing document`}
		} else {
			if(response != null){
				return response
			}
		}
	})
}

// GET A TASK
module.exports.getTask = async (id) => {
	return await Task.findById(id).then( (result, err) => {
		if(result){
			return result
		} else {
			return err
		}
	})
}

// UPDATE A SPECIFIC TASK TO CHANGE STATUS TO COMPLETE
module.exports.updateTaskById = async (id) => {
	return await Task.findByIdAndUpdate(id, {$set: {status: "Complete"}}, {new:true})
	.then(response => {
        // console.log(response)
		if(response == null){
			return {message: `no existing document`}
		} else {
			if(response != null){
				return response
			}
		}
	})
}