const express = require('express');
const router = express.Router();

const {
	createTask,
	getAllTasks,
	deleteTask,
	updateTask,
    getTask,
    updateTaskById

} = require('./../controllers/taskControllers')

//CREATE A TASK
router.post('/', async (req, res) => {
	try{
		await createTask(req.body).then(result => res.send(result))
	} catch(err){
		res.status(400).json(err.message)
	} 
})

//GET ALL TASKS
router.get('/', async (req, res) => {
	try{
		await getAllTasks().then(result => res.send(result))
	} catch(err){
		res.status(500).json(err)
	}
})

//DELETE A TASK
router.delete('/:id/delete', async (req, res) => {
	try{
		await deleteTask(req.params.id).then(response => res.send(response))
	}catch(err){
		res.status(500).json(err)
	}
})

//UPDATE A TASK
router.put('/', async (req, res) => {
	await updateTask(req.body.name, req.body).then(response => res.send(response))
})


// GET A SPECIFIC TASK
router.get('/:taskId', async (req, res) => {
	await getTask(req.params.id).then(result => res.send(result))
})


// UPDATE A SPECIFIC TASK TO CHANGE STATUS TO COMPLETE
router.put('/:taskId/update', async (req, res) => {
	await updateTaskById(req.params.id).then(response => res.send(response))
})


module.exports = router;